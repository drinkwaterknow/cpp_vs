﻿#include <iostream>

template<typename T> class Stack
{
private:
    T* stack;
    int current_index = 0;
    int length = 1;
public:
    Stack() : stack(nullptr), current_index(0), length(0)
    {}

    Stack(T element) : current_index(1), length(1)
    {
        this->stack = new T[1];
        this->stack[0] = element;
    }

    void push(T element)
    {
        if (this->length > this->current_index)
        {
            stack[this->current_index] = element;
        }
        else {
            if (this->length > 0) {
                this->length *= 2;
            }
            else {
                this->length = 1;
            }
            T* newStack = new T[this->length];
            for (int i = 0; i < this->current_index; i++)
            {
                newStack[i] = this->stack[i];
            }
            newStack[this->current_index] = element;
            delete[] this->stack;
            this->stack = newStack;
        }
        this->current_index += 1;
    }

    T pop()
    {
        if (this->current_index > 0)
        {
            this->current_index -= 1;
            return this->stack[this->current_index];
        }
        std::cout << "Stack is empty!" << std::endl;
        return NULL;
    }

    void print()
    {
        for (int i = 0; i < this->current_index; i++) {
            std::cout << this->stack[i] << std::endl;
        }
    }
};

int main()
{
    Stack<int> a(10);
    a.push(5);
    a.print();
    a.push(6);
    a.print();
    std::cout << a.pop() << std::endl;
    a.print();
    std::cout << a.pop() << std::endl;
    std::cout << a.pop() << std::endl;
    a.pop();
}